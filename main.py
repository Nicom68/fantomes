from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector


mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database='fantomes'
)

cursor = mydb.cursor()



def get_page_content(url):
    url = url.replace('..', "")
    return urlopen("https://www.paranormaldatabase.com"+url).read()

# get content home
html_home = get_page_content("/index.html")

# get category home
list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    if lien:
        url= lien.get('href')
        list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    if lien:
        url= lien.get('href')
        list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    if lien:
        url= lien.get('href')
        list_cat_home.append(url)


for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()


    
    
    list_sous_cat = []
   
    for ss_cat in list_sous_cat:
        html_sous_cat = get_page_content(ss_cat)
        soup = BeautifulSoup(html_sous_cat, features="html.parser")
        # prendre plutot le titre h3 de la page des cas
        titre_ss_cat = soup.select_one(".w3-border > h4:nth-child(1)")
        titre_ss_cat.find("a").extract()
        titre_ss_cat.find("a").extract()
        titre_ss_cat = titre_ss_cat.getText()
        titre_ss_cat = titre_ss_cat.replace("> ", "")
        titre_ss_cat = titre_ss_cat.strip()
        list_sous_cat.append(titre_ss_cat)
    
        liste_thirds = soup.find_all(class_="w3-third")
        for third in liste_thirds:
            lien = third.find('a')
        if lien:
            url= lien.get('href')
        list_sous_cat.append(url)

        liste_halfs = soup.find_all(class_="w3-half")
        for third in liste_halfs:
            lien = third.find('a')
        if lien:
            url= lien.get('href')
            list_sous_cat.append(url)
    
            print(list_sous_cat)


 # refaire tout pour chaque page de la pagination
        qs = "?"
        while(qs):
            html_sous_cat = get_page_content(ss_cat + qs)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")

        list_cas = soup.select("div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")

        for cas in list_cas:
                # parser le cas

                # title varchar(255),
            titre_cas = cas.select_one("h4").get_text()

                # recup le fameux P
            p = cas.select_one("p:nth-last-child(1)")
            p_text = p.get_text().split("\n")
            p_text[0] = p_text[0].replace("Location: ", "")
                # type varchar(255),
            p_text[1] = p_text[1].replace("Type: ", "")
                # date varchar(255),
            p_text[2] = p_text[2].replace("Date / Time: ", "")
                # comments varchar(255),
            p_text[3] = p_text[3].replace("Further Comments: ", "")

                
                
        if titre_cat == "Calendar":
                    pass
                    # month dans la cat calendar
                    # weather dans la cat calencar

                # insert into
                
              

# Insertion des données dans la table "catégorie"
        cursor.execute("SELECT id FROM categorie WHERE name = %s", (titre_cat,))
        existing_cat = cursor.fetchone()

        if existing_cat:
    # Si la catégorie existe, obtenir son ID
            cat_id = existing_cat[0]
        else:
    # Si la catégorie n'existe pas, l'insérer dans la base de données
            cat_type = 'reports' if 'Reports' in titre_cat else 'geographic'
            cursor.execute("INSERT INTO categorie (name, parent, cat_type) VALUES (%s, NULL, %s)",(titre_cat,cat_type))    # Récupérer l'ID de la nouvelle catégorie
            cat_id = cursor.lastrowid


# next
            nav_btn = soup.select(".w3-quarter.w3-container h5")
            qs = None
            for btn in nav_btn:
                if btn.get_text().find('next') > 0:
                    qs = btn.select_one("a").get("href")

        mydb.commit()
        cursor.close()
        mydb.close()
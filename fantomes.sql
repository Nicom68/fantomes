DROP DATABASE IF EXISTS fantomes;
CREATE DATABASE fantomes;
USE fantomes;

GRANT ALL PRIVILEGES ON fantomes.* TO 'nicolas'@'localhost';

CREATE TABLE cas (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title varchar(255),
  location varchar(255),
  type varchar(255),
  date varchar(255),
  comment text,
  month int,
  weather bool
);

CREATE TABLE categorie_cas (
  id_cas int,
  id_element int,
  PRIMARY KEY (id_cas, id_element)
);

CREATE TABLE categorie (
  id int AUTO_INCREMENT PRIMARY KEY,
  name varchar(255),
  parent int NULL,
  cat_type enum ('reports', 'geographic')
);





